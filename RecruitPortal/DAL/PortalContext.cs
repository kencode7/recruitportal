﻿using RecruitPortal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RecruitPortal.DAL
{
    public class PortalContext : DbContext
    {
        public PortalContext() : base("PortalContext")
        {
            Database.SetInitializer<PortalContext>(new DropCreateDatabaseIfModelChanges<PortalContext>());
        }

        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Position> Positions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
    public class PortalDbInitializer : DropCreateDatabaseIfModelChanges<PortalContext>
    {
        protected override void Seed(PortalContext context)
        {
           base.Seed(context);
        }
    }
}