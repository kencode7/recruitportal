﻿namespace RecruitPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatePositionTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Devops Engineer', 'Junior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('FrontEnd Engineer', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('ASP.NET Developer', 'Mid-level')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('project Manager', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Backend Engineer', 'Intermediate')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('ReactNative Engineer', 'Junior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Javascript Engineer', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Flutter Developer', 'Mid-level')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('PHP Engineer', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Ruby Engineer', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('CI/CD Engineer', 'Senior')");
            Sql("INSERT INTO Positions (Name, Level) VALUES ('Net Developer', 'Junior')");
        }
        
        public override void Down()
        {
        }
    }
}
