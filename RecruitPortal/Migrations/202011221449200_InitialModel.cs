﻿namespace RecruitPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applicants",
                c => new
                    {
                        ApplicantID = c.Int(nullable: false, identity: true),
                        ApplicantName = c.String(),
                        ApplicantEmail = c.String(),
                        PhoneNumber = c.Short(nullable: false),
                        StateResidence = c.String(),
                        ExperienceLevel = c.String(),
                        PortfolioLink = c.String(),
                        Photo = c.Binary(),
                        ResumeUpload = c.Binary(),
                    })
                .PrimaryKey(t => t.ApplicantID);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Level = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Positions");
            DropTable("dbo.Applicants");
        }
    }
}
