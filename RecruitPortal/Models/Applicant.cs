﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecruitPortal.Models
{
    public class Applicant
    {
        public int ApplicantID { get; set; }
        public string ApplicantName { get; set; }
        public string ApplicantEmail { get; set; }
        public short PhoneNumber { get; set; }
        public string StateResidence { get; set; }
        public string ExperienceLevel { get; set; }
        public string PortfolioLink { get; set; }
        public byte[] Photo { get; set; }
        public byte[] ResumeUpload { get; set; }

    }
}